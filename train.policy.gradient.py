from env import step, init, transform
import tensorflow as tf
import numpy as np

import policyGradient as p

sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

rewards = []
for i in range(int(1e3)):
    s = init()
    memory = []
    while not s['terminal']:
        st = transform(s)
        action_prob = sess.run(p.action_prob, feed_dict={p.feature:[st]})
        print(action_prob)
        a = 'hit' if action_prob[0][0] > np.random.rand() else 'stick'
        #print(a)
        s,reward = step(s,a)
        memory.append([st, a, reward])
    if len(rewards) > 100:
        rewards.pop(0)
    rewards.append(memory[-1][2])
    print('{}th episode with average reward {}'.format(i+1,np.mean(rewards)))

    for i, unit in enumerate(memory):
        G = np.sum([item[2] for j,item in enumerate(memory[i:])])
        unit.append(G)

    states, actions, _, returns = list(zip(*memory))
    binaryActions = [[1,0] if x=='hit' else [0,1] for x in actions]
    returns = [[x] for x in returns]

    sess.run(p.train_action,feed_dict={p.feature:states,
    p.action:binaryActions, p.target:returns})
    sess.run(p.train_value,feed_dict={p.feature:states,
    p.target:returns})

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

v = np.zeros((21,10))
acs = []
for i in range(21):
    for j in range(10):
        for k in range(2):
            s = {'dealer':j+1,'player':i+1}
            st = transform(s)
            #print(st)
            acs.append(sess.run(p.action_prob,feed_dict={p.feature:[st]})[0])
            v[i,j] = sess.run(p.value,feed_dict={p.feature:[st]})

fig = plt.figure()
ax = fig.add_subplot(121, projection='3d')
dealer, player = np.meshgrid(range(1,11),range(1,22))
ax.plot_wireframe(dealer,player,v)
