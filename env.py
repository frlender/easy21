import numpy as np
import copy

def getCard():
    val = np.random.randint(1,11)
    reference = np.random.randint(1,4)
    if reference == 1:
        color = 'red'
    else:
        color = 'black'
    return val, color


def updateCard(s):
    card = getCard()
    if card[1] == 'red':
        s -= card[0]
    else:
        s += card[0]
    return s

def isBust(s):
    return s > 21 or s < 1

#s: 'player','dealer','terminal'
def step(s,a):
    s = copy.deepcopy(s)
    if a=='hit':
        s['player'] = updateCard(s['player'])
        if isBust(s['player']):
            s['terminal'] = True
            return s, -1
        else:
            return s, 0
    else:
        s['terminal'] = True
        while s['dealer']>=1 and s['dealer']<=17:
            s['dealer'] = updateCard(s['dealer'])
        if isBust(s['dealer']):
            return s, 1
        else:
            if s['dealer'] > s['player']:
                return s, -1
            elif s['dealer'] == s['player']:
                return s, 0
            else:
                return s, 1

def getBlackCard():
    while True:
        card = getCard()
        if card[1] == 'black':
            break
    return card

def init():
    s = {'player':getBlackCard()[0],'dealer':getBlackCard()[0],'terminal':False}
    return s

def _isInRange(num,arange):
    return num>=arange[0] and num<=arange[1]

def transform(s):
    valRange = {'dealer':[(1,4),(4,7),(7,10)],
    'player':[(1,6),(4,9),(7,12),(10,15),(13,18),(16,21)]}
    dealerFeature = [int(_isInRange(s['dealer'],item)) for item in valRange['dealer']]
    playerFeature = [int(_isInRange(s['player'],item)) for item in valRange['player']]
    return dealerFeature + playerFeature

def transform2(s):
    valRange = {'dealer':[(1,4),(4,7),(7,10)],
    'player':[(1,6),(4,9),(7,12),(10,15),(13,18),(16,21)]}
    dealerFeature = np.array([_isInRange(s['dealer'],item) 
    for item in valRange['dealer']])
    playerFeature = np.array([_isInRange(s['player'],item) 
    for item in valRange['player']])
    feature = np.zeros((len(valRange['dealer']),len(valRange['player'])))
    feature[dealerFeature, playerFeature] = 1
    return feature.ravel()
