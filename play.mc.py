from env import step, init, transform2
import lib
import numpy as np
import tensorflow as tf
import policyGradient2 as p

agent = lib.Agent(plot=False, q='mc-agent-q.pkl')
R = []
for i in range(int(1e4)):
    s = init()
    r = 0
    while not s['terminal']:
        a = agent.play(s)
        s,reward = step(s,a)
        r += reward
    R += [reward]
print('Performance MC agent:', np.mean(R), np.std(R))

sarsaLinearAgent = lib.SarsaLinearAgent(plot=False,
                             theta='sarsa-linear-lambda-0.9.pkl')
R = []
for i in range(int(1e4)):
    s = init()
    r = 0
    while not s['terminal']:
        a = agent.play(s)
        s,reward = step(s,a)
        r += reward
    R += [reward]
print('Performance sarsaLinearAgent agent:', np.mean(R), np.std(R))



sess = tf.InteractiveSession()
saver = tf.train.Saver()
saver.restore(sess, "/tmp/easy21-model.ckpt")
R = []
for i in range(int(1e4)):
    s = init()
    r = 0
    while not s['terminal']:
        st = transform2(s)
        action_prob = sess.run(p.action_prob, feed_dict={p.feature:[st]})
        a = 'hit' if action_prob[0][0] > np.random.rand() else 'stick'
        s,reward = step(s,a)
        r += reward
    R += [reward]
print('Performance sarsaLinearAgent agent:', np.mean(R), np.std(R))
