from env import step, init
from lib import SarsaLinearAgent
import numpy as np
import matplotlib.pyplot as plt


agents = []
for n, lda in enumerate(np.arange(0,1.1,0.1)):
    agent = SarsaLinearAgent(lda=lda,trueQ='mc-agent-q.pkl')
    agents.append(agent)
    for i in range(int(1e5)):
        s = init()
        episode = {'seq':[],'reward':None}
        while not s['terminal']:
            a = agent.act(s)
            episode['seq'].append((s,a))
            s,reward = step(s,a)
        episode['reward'] = reward
        agent.learn(episode)
        if (i+1)%int(1e4) == 0:
            print('{:.1e}'.format(i))
            agent.expose()
        if i%int(1e3) == 0:
            print('{:.1e}'.format(i))
            agent.plotMSE(i)

plt.figure()
plt.plot([agent.lda for agent in agents],
        [agent.mse[-1][1] for agent in agents])

plt.plot([x[0] for x in agents[0].mse],
[x[1] for x in agents[0].mse])
plt.plot([x[0] for x in agents[10].mse],
[x[1] for x in agents[10].mse])
plt.plot([x[0] for x in agents[5].mse],
[x[1] for x in agents[5].mse])

import pickle
with open('sarsa-linear-lambda-0.9.pkl','wb') as sf:
    pickle.dump(agents[-2].theta,sf)