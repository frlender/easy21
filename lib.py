import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pickle
import copy

class Agent:
    def __init__(self,n0=100, plot=True, q=None):
        self.q = np.zeros((21,10,2))
        self.qCount = np.zeros((21,10,2))
        self.sCount = np.zeros((21,10))
        self.epsilon = np.ones((21,10))
        self.n0 = n0

        if q:
            self.loadQ(q)

        if plot:
            # # for mesh plot
            self.fig = plt.figure()
            self.ax = self.fig.add_subplot(121, projection='3d')


    def _updateEpsilon(self):
        self.epsilon = self.n0/(self.n0+self.sCount)

    def loadQ(self,path):
        with open(path,'rb') as pf:
            self.q = pickle.load(pf)

    def learn(self,episode):
        for item in episode['seq']:
            idx = self._getIdx(item[0],item[1])
            self.qCount[idx[0],idx[1],idx[2]] += 1
            self.sCount[idx[0],idx[1]] += 1
            self.q[idx[0],idx[1],idx[2]] += 1/self.qCount[idx[0],
            idx[1],idx[2]]*(episode['reward']-self.q[idx[0],idx[1],
            idx[2]])
        self._updateEpsilon()

    def _idx2action(self,idx):
        return 'stick' if idx else 'hit'

    def play(self,s):
        idx = self._getIdx(s)
        q = self.q[idx[0],idx[1]]
        epsilon = self.epsilon[idx[0],idx[1]]
        bestAction = np.argmax(q)
        return self._idx2action(bestAction)

    def act(self,s):
        idx = self._getIdx(s)
        q = self.q[idx[0],idx[1]]
        epsilon = self.epsilon[idx[0],idx[1]]
        bestAction = np.argmax(q)
        bestActionP = epsilon/2+1-epsilon
        ref = np.random.rand(1)[0]
        if ref < bestActionP:
            return self._idx2action(bestAction)
        else:
            return self._idx2action(1-bestAction)

    def _getIdx(self,s,a=None):
        if a==None:
            return s['player']-1, s['dealer']-1
        else:
            if a== 'hit':
                aIdx = 0
            else:
                aIdx = 1
            return s['player']-1, s['dealer']-1, aIdx

    def save(self):
        with open(self.__class__.__name__+'-agent-q.pkl','wb') as mf:
            pickle.dump(self.q,mf)

    def expose(self):
        self.ax.cla()
        dealer, player = np.meshgrid(range(1,11),range(1,22))
        za = np.amax(self.q,axis=2)[player-1,dealer-1]
        self.ax.plot_wireframe(dealer,player,za)
        plt.pause(0.01)

class SarsaAgent(Agent):
    def __init__(self,lda,trueQ,n0=100):
        super().__init__(n0)
        self.lda = lda
        self.loadTrueQ(trueQ)
        self.mse = []
        # for MSE plot
        self.fig.add_subplot(122)

    def loadTrueQ(self,path):
        with open(path,'rb') as pf:
            self.trueQ = pickle.load(pf)

    def learn(self,episode):
        def getQ(s,a):
            idx = self._getIdx(s,a)
            return self.q[idx[0],idx[1],idx[2]]
        qs = [getQ(s,a) for (s,a) in episode['seq']] + [episode['reward']]
        qs = qs[1:]

        for i,(s,a) in enumerate(episode['seq']):
            idx = self._getIdx(s,a)
            q = self.q[idx[0],idx[1],idx[2]]
            self.qCount[idx[0],idx[1],idx[2]] += 1
            self.sCount[idx[0],idx[1]] += 1

            lda = self.lda
            qsAfter = qs[i:]
            targets = [(1-lda)*val*pow(lda,j) if j<len(qsAfter)-1 else
            val*pow(lda,j) for j,val in enumerate(qsAfter)]
            target = sum(targets)

            self.q[idx[0],idx[1],idx[2]] += 1/self.qCount[idx[0],
            idx[1],idx[2]]*(target-q)

            self.epsilon[idx[0],idx[1]] = self.n0/(self.n0+
            self.sCount[idx[0],idx[1]])

    def getMSE(self):
        return np.mean(np.power(self.trueQ - self.q, 2))

    def plotMSE(self,i):
        plt.subplot(122)
        mse = self.getMSE()
        self.mse.append([i,mse])
        plt.cla()
        plt.title('MSE, lambda = ' + str(self.lda))
        plt.plot([x[0] for x in self.mse],[x[1] for x in self.mse])
        plt.pause(0.01)

class SarsaLinearAgent(SarsaAgent):
    def __init__(self,lda=None,trueQ=None,plot=True, theta=None):
        if plot:
            self.fig = plt.figure()
            self.ax = self.fig.add_subplot(121, projection='3d')
            self.fig.add_subplot(122)
        if trueQ:
            self.loadTrueQ(trueQ)
        if lda:
            self.lda = lda
        self.epsilon = 0.05
        self.alpha = 0.01
        self.mse = []

        self.range = {'dealer':[(1,4),(4,7),(7,10)],
        'player':[(1,6),(4,9),(7,12),(10,15),(13,18),(16,21)],
        'action':['hit','stick']}
        self.featureSize = [len(self.range['dealer']),
        len(self.range['player']),len(self.range['action'])]

        if theta:
            self.theta = self.load(theta)
        else:
            self.theta = np.zeros(np.prod(self.featureSize))
        self.qf = np.zeros((21,10,2,np.prod(self.featureSize)))

        for i in range(21):
            for j in range(10):
                for k in range(2):
                    s = {'dealer':j+1,'player':i+1}
                    a = self._idx2action(k)
                    self.qf[i,j,k] = self.getFeature(s,a)

    def _isInRange(self,num,arange):
        return num>=arange[0] and num<=arange[1]

    def load(self,pathx):
        with open(pathx,'rb') as pf:
            return pickle.load(pf)

    def getFeature(self,s,a):
        feature = np.zeros(self.featureSize)
        dealer = np.array([self._isInRange(s['dealer'],x) for
        x in self.range['dealer']])
        player = np.array([self._isInRange(s['player'],x) for
        x in self.range['player']])
        action = np.array([a==x for x in self.range['action']])
        feature[dealer,player,action] = 1
        return feature.ravel()

    def getQ(self,s,a):
        feature = self.getFeature(s,a)
        return np.dot(feature,self.theta)

    def learn(self,episode):
        qs = [self.getQ(s,a) for (s,a) in episode['seq']] + [episode['reward']]
        qs = qs[1:]

        for i,(s,a) in enumerate(episode['seq']):
            q = self.getQ(s,a)
            feature = self.getFeature(s,a)

            lda = self.lda
            qsAfter = qs[i:]
            targets = [(1-lda)*val*pow(lda,j) if j<len(qsAfter)-1 else
            val*pow(lda,j) for j,val in enumerate(qsAfter)]
            target = sum(targets)

            self.theta += self.alpha*(target-q)*feature

    def act(self,s):
        qs = [self.getQ(s,a) for a in self.range['action']]
        epsilon = self.epsilon
        bestAction = np.argmax(qs)
        bestActionP = epsilon/2+1-epsilon
        ref = np.random.rand(1)[0]
        if ref < bestActionP:
            return self._idx2action(bestAction)
        else:
            return self._idx2action(1-bestAction)

    def getMSE(self):
        q = np.dot(self.qf,self.theta)
        return np.mean(np.power(self.trueQ - q, 2))

    def expose(self):
        self.ax.cla()
        dealer, player = np.meshgrid(range(1,11),range(1,22))
        q = np.dot(self.qf,self.theta)
        za = np.amax(q,axis=2)[player-1,dealer-1]
        self.ax.plot_wireframe(dealer,player,za)
        plt.pause(0.01)

    def save(self):
        pass
