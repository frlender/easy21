from env import step, init
from lib import Agent

agent = Agent()
for i in range(int(1e7)):
    s = init()
    episode = {'seq':[],'reward':None}
    while not s['terminal']:
        a = agent.act(s)
        episode['seq'].append((s,a))
        s,reward = step(s,a)
    episode['reward'] = reward
    agent.learn(episode)
    if (i+1)%int(1e4) == 0:
        print('{:.1e}'.format(i))
        agent.expose()
agent.save()
