import tensorflow as tf

value_lr = 0.1
target_lr = 0.1

with tf.name_scope('input'):
    feature = tf.placeholder(tf.float32, [None,9], 'feature')
    target = tf.placeholder(tf.float32, [None,1], 'target')
    with tf.name_scope('action_net'):
        action = tf.placeholder(tf.float32, [None,2], 'action')

with tf.name_scope('value_net'):
    weights_v1 = tf.Variable(tf.truncated_normal([9,18], stddev=0.1),
    name='weights_v1')
    biases_v1 = tf.Variable(tf.constant(0.1, shape=[18]), name='biases_v1')
    preactivation_v1 = tf.matmul(feature, weights_v1) + biases_v1
    activation_v1 = tf.nn.relu(preactivation_v1, name='activation_v1')

    weights_v2 = tf.Variable(tf.truncated_normal([18,1], stddev=0.1),
    name='weights_v2')
    biases_v2 = tf.Variable(tf.constant(0.1), name='biases_v2')
    value = tf.matmul(activation_v1, weights_v2) + biases_v2
    mse = tf.contrib.losses.mean_squared_error(value, target)
    train_value = tf.train.AdamOptimizer(learning_rate=value_lr)\
    .minimize(mse)

with tf.name_scope('action_net'):
    weights_a1 = tf.Variable(tf.truncated_normal([9,18], stddev=0.1),
    name='weights_a1')
    biases_a1 = tf.Variable(0.1 , name='biases_a1')
    # None x 1 = None x 4 x 4 x 1
    preactivation_a1 = tf.matmul(feature, weights_a1) + biases_a1
    activation_a1 = tf.nn.relu(preactivation_a1, name='activation_a1')

    weights_a2 = tf.Variable(tf.truncated_normal([18,2], stddev=0.1),
    name='weights_a2')
    biases_a2 = tf.Variable(tf.constant(0.1), name='biases_a2')
    preactivation_a2 = tf.matmul(activation_a1, weights_a2) + biases_a2

    action_prob = tf.nn.softmax(preactivation_a2, name='action_prob')


    with tf.name_scope('gradient'):
        chosen_action_prob = tf.reduce_sum(tf.multiply(action_prob, action),
        axis=1, keep_dims=True)
        log_prob = tf.log(chosen_action_prob)
        J = tf.multiply(log_prob, target-value)
        loss = - tf.reduce_mean(J)
        train_action = tf.train.AdamOptimizer(learning_rate=target_lr)\
        .minimize(loss)
